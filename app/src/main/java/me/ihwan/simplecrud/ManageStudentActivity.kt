package me.ihwan.simplecrud

import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.view.View
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import kotlinx.android.synthetic.main.activity_manage_student.*
import org.json.JSONObject

class ManageStudentActivity : AppCompatActivity() {

    lateinit var i: Intent
    private var gender = "Pria"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_student)

        i = intent

        if(i.hasExtra("editmode")){

            if(i.getStringExtra("editmode").equals("1")){

                onEditMode()

            }

        }
        genderGroup.setOnCheckedChangeListener { radioGroup, i ->

            when(i){

                R.id.boy->{
                    gender = "Pria"
                }

                R.id.girl->{
                    gender = "Wanita"
                }

            }

        }

        create.setOnClickListener {
            create()
        }

        update.setOnClickListener {
            update()
        }

        delete.setOnClickListener {
            AlertDialog.Builder(this)
                    .setTitle("Konfirmasi")
                    .setMessage("Hapus data ini ?")
                    .setPositiveButton("HAPUS", DialogInterface.OnClickListener { dialogInterface, i ->
                        delete()
                    })
                    .setNegativeButton("BATAL",DialogInterface.OnClickListener { dialogInterface, i ->
                        dialogInterface.dismiss()
                    })
                    .show()
        }
    }

    private fun onEditMode(){

        nim.setText(i.getStringExtra("nim"))
        name.setText(i.getStringExtra("name"))
        address.setText(i.getStringExtra("address"))
        nim.isEnabled = false

        create.visibility = View.GONE
        update.visibility = View.VISIBLE
        delete.visibility = View.VISIBLE

        gender = i.getStringExtra("gender")

        if(gender.equals("Pria")){
            genderGroup.check(R.id.boy)
        }else{
            genderGroup.check(R.id.girl)
        }

    }

    private fun create(){
        val loading = ProgressDialog(this)
        loading.setMessage("Menambahkan data...")
        loading.show()

        AndroidNetworking.post(ApiEndPoint.CREATE)
                .addBodyParameter("nim",nim.text.toString())
                .addBodyParameter("name",name.text.toString())
                .addBodyParameter("address",address.text.toString())
                .addBodyParameter("gender",gender)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {

                    override fun onResponse(response: JSONObject?) {
                        loading.dismiss()
                        Toast.makeText(applicationContext,response?.getString("message"),Toast.LENGTH_SHORT).show()

                        if(response?.getString("message")?.contains("successfully")!!){
                            this@ManageStudentActivity.finish()
                        }
                    }

                    override fun onError(anError: ANError?) {
                        loading.dismiss()
                        Log.d("ONERROR",anError?.errorDetail?.toString())
                        Toast.makeText(applicationContext,"Connection Failure",Toast.LENGTH_SHORT).show()
                    }


                })
    }

    private fun update(){

        val loading = ProgressDialog(this)
        loading.setMessage("Mengubah data...")
        loading.show()

        AndroidNetworking.post(ApiEndPoint.UPDATE)
                .addBodyParameter("nim",nim.text.toString())
                .addBodyParameter("name",name.text.toString())
                .addBodyParameter("address",address.text.toString())
                .addBodyParameter("gender",gender)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {

                    override fun onResponse(response: JSONObject?) {

                        loading.dismiss()
                        Toast.makeText(applicationContext,response?.getString("message"),Toast.LENGTH_SHORT).show()

                        if(response?.getString("message")?.contains("successfully")!!){
                            this@ManageStudentActivity.finish()
                        }

                    }

                    override fun onError(anError: ANError?) {
                        loading.dismiss()
                        Log.d("ONERROR",anError?.errorDetail?.toString())
                        Toast.makeText(applicationContext,"Connection Failure", Toast.LENGTH_SHORT).show()                    }


                })

    }

    private fun delete(){

        val loading = ProgressDialog(this)
        loading.setMessage("Menghapus data...")
        loading.show()

        AndroidNetworking.get(ApiEndPoint.DELETE+"?nim="+nim.text.toString())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {

                    override fun onResponse(response: JSONObject?) {

                        loading.dismiss()
                        Toast.makeText(applicationContext,response?.getString("message"),Toast.LENGTH_SHORT).show()

                        if(response?.getString("message")?.contains("successfully")!!){
                            this@ManageStudentActivity.finish()
                        }

                    }

                    override fun onError(anError: ANError?) {
                        loading.dismiss()
                        Log.d("ONERROR",anError?.errorDetail?.toString())
                        Toast.makeText(applicationContext,"Connection Failure", Toast.LENGTH_SHORT).show()                    }


                })

    }


}
