package me.ihwan.simplecrud

class ApiEndPoint {
    companion object {

        //url server
        private val SERVER = "https://blog.ihwan.me/api/"
        val CREATE = SERVER+"create.php"
        val READ = SERVER+"read.php"
        val DELETE = SERVER+"delete.php"
        val UPDATE = SERVER+"update.php"

    }
}